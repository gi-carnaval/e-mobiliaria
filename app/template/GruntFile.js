module.exports = function(grunt) {

    grunt.initConfig({
        //copy folders
         copy: {
            dist: {
                files: [
                    {expand: true, cwd: 'ltr-horizontal/', src: '**', dest: 'rtl-horizontal/'},
                    {expand: true, cwd: 'ltr-vertical/', src: '**', dest: 'rtl-vertical/'}
                ]
            }
        },
        // minify all js files
      uglify: {
          files:{ 
                  src: 'ltr-horizontal/assets/js/*.js',  // source files mask
                  dest: 'ltr-horizontal/assets/js/',    // destination folder
                  expand: true,    // allow dynamic building
                  flatten: true,   // remove all unnecessary nesting
                  ext: '.min.js'   // replace .js to .min.js
          },
          files2:{ 
                  src: 'ltr-vertical/assets/js/*.js',  // source files mask
                  dest: 'ltr-vertical/assets/js/',    // destination folder
                  expand: true,    // allow dynamic building
                  flatten: true,   // remove all unnecessary nesting
                  ext: '.min.js'   // replace .js to .min.js
          },
          files3:{ 
                  src: 'rtl-horizontal/assets/js/*.js',  // source files mask
                  dest: 'rtl-horizontal/assets/js/',    // destination folder
                  expand: true,    // allow dynamic building
                  flatten: true,   // remove all unnecessary nesting
                  ext: '.min.js'   // replace .js to .min.js
          },
          files4:{ 
                  src: 'rtl-vertical/assets/js/*.js',  // source files mask
                  dest: 'rtl-vertical/assets/js/',    // destination folder
                  expand: true,    // allow dynamic building
                  flatten: true,   // remove all unnecessary nesting
                  ext: '.min.js'   // replace .js to .min.js
          }

      },     
      watch: {
          js:  { files: ['ltr-horizontal/assets/js/*.js'], tasks: [ 'uglify:files' ] },
          js:  { files: ['ltr-vertical/assets/js/*.js'], tasks: [ 'uglify:files2' ] },
          js:  { files: ['rtl-horizontal/assets/js/*.js'], tasks: [ 'uglify:files3' ] },
          js:  { files: ['rtl-vertical/assets/js/*.js'], tasks: [ 'uglify:files4' ] },
         
      },
      //minify all css files
    cssmin: {
      target: {
        files: [
          {expand: true,cwd: 'ltr-horizontal/assets/css/',src: ['**/*.css', '!*.min.css'],dest: 'ltr-horizontal/assets/css/',ext: '.min.css'}
        ]
      },
      target1:{
        files:[
          {expand: true,cwd: 'ltr-vertical/assets/css/',src: ['**/*.css', '!*.min.css'],dest: 'ltr-vertical/assets/css/',ext: '.min.css'}
        ]
      },
      target2: {
        files: [
          {expand: true,cwd: 'rtl-horizontal/assets/css/',src: ['**/*.css', '!*.min.css'],dest: 'rtl-horizontal/assets/css/',ext: '.min.css'}
        ]
      },
      target3:{
        files:[
          {expand: true,cwd: 'rtl-vertical/assets/css/',src: ['**/*.css', '!*.min.css'],dest: 'rtl-vertical/assets/css/',ext: '.min.css'}
        ]
      },
    }
    });

   // load plugins
   grunt.loadNpmTasks('grunt-contrib-copy');
   grunt.loadNpmTasks('grunt-contrib-watch');
   grunt.loadNpmTasks('grunt-contrib-uglify');
   grunt.loadNpmTasks('grunt-contrib-cssmin');

    // register at least this one task
   grunt.registerTask('default', [ 'copy','uglify','cssmin:target','cssmin:target1','cssmin:target2','cssmin:target3']);
   
};
