<?php

/**
 * Conecta com o banco de dados
 *
 * @return object - Retorna um objeto que representa a conexÃ£o com um servidor MySQL .
 */

function  conectaDB()
{
    //mysqli_connect ($host,$username,$passwd,$dbname,$port,$socket) : mysqli|false
    // Inter Academy QA
    // $db = mysqli_connect("srv.remsoft.com.br", "remsoftc_inter_academy_qa", "(YK!QL+ukCC;", "remsoftc_inter_academy_qa");
    // Inter Academy local
    $db = mysqli_connect("localhost", "root", "", "emobiliaria");

    // se ocorrer um erro printa a conexão falhou 
    if (mysqli_connect_errno()) printf("A conexão falhou: %s\n", mysqli_connect_error());
    return $db;
}
