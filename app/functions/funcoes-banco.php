<?php

// fazendo requerimento do arquvio db.php
require "db.php";

// conectando com o banco de dados 
$db = conectaDB();


// ------------------ PORQUE VOCES FAZEM OS CRUDS DE DUAS FORMAS --------------------------- 


$GLOBALS["pre"] = "";

/**

 * Insere informações no banco de dados.

 *

 * @param string $query - A query que você deseja para inserir no banco de dados

 * @param bool $retornarId - Retorna o id inserido - @default false

 * @param bool $echo - Mostra a query inserida - @default false

 *

 * @return bool/int - Retorna se deu certo ou errado a query. (Caso $retornarId seja true, retorna o id inserido)

 */

function inserirPadrao($query, $retornarId = false, $echo = false)
{

	// caso verdadeiro vai retornar a query inserida
	if ($echo) echo $query . "<br /><br />";

	// mysqli_query ( mysqli $mysql , string $query)
	$ins = mysqli_query($GLOBALS["db"], $query);

	// mysqli_affected_rows — Obtem o numero de fileiras afetadas em uma operação mysql previa
	if (mysqli_affected_rows($GLOBALS["db"]) != -1) {

		// se o retornaId for verdadeiro 
		if ($retornarId) {

			// se o echo for verdadeiro, vai fazer um echo do ultimo id inserido 
			if ($echo) echo "SELECT LAST_INSERT_ID();<br /><br />";

			$sel = mysqli_query($GLOBALS["db"], "SELECT LAST_INSERT_ID() AS id;");

			$fet = mysqli_fetch_array($sel);

			$id = $fet['id'];
		}

		// se for para retornar o id, retornamos 
		if ($retornarId) return $id;

		// se não for necessario mas a operação tiver sido um sucesso, retornamos true
		else return true;
	}

	// se ocrreu algum erro retornamos false
	return false;
}

/**

 * Insere informações no banco de dados.

 *

 * @param string $tabela - O nome da tabela onde vai inserir

 * @param array(string) $campos - Os campos que serão inseridos na base de dados

 * @param array(string) $valores - Os valores dos campos que serão inseridos na base de dados (Deve ter a mesma 

	quantidade do array da variavél de campos)

 * @param bool $retornarId - Retorna o id inserido - @default false	

 * @param bool $echo - Mostra a query inserida - @default false

 *

 * @return bool/int - Retorna se deu certo ou errado a query. (Caso $retornarId seja true, retorna o id inserido)

 */

function inserir($tabela, $campos, $valores, $retornarId = false, $echo = false)
{

	// ------------------ PORQUE VOCES FAZEM ESSE TRATAMENTO DE STRING COM OS CAMPOS ---------------------------

	// variavel c vazia 
	$c = '';

	// campo é um array, para cada campo como sendo valor
	foreach ($campos as $valor) {

		// trocamos o espaço por !@#
		$c .= utf8_decode(str_replace(" ", "!@#", $valor) . ' ');
	}

	// em seguida trocamos os espaços por espaços com virgula 
	// trim() - Retire os espaços em branco (ou outros caracteres) do início e do final de uma string
	$c = str_replace(" ", ", ", trim($c));

	// trocamos os !@# por espaços na variavel $c
	$c = str_replace("!@#", " ", $c);

	// varaivel v vazia 
	$v = '';

	// valores é um array, para cada valor como valor
	foreach ($valores as $valor) {

		// se o valor não for numerico 
		if (!is_numeric($valor))

			// se o valor for vazio o valor = NULL
			if ($valor == '') $valor = 'NULL';

		// se o valor for now ou null ou 
		// strpos — Econtra a posição da primeira ocorrencia de uma substring em uma string
		// se achar DATE_ADD na string valor, o strpos vai retornar true, se for diferente de falso 
		// caimos no if
		if ($valor == 'now()' || $valor == 'NULL' || @strpos($valor, 'DATE_ADD') !== false)

			// utf8_decode - Converte uma string com caracteres ISO-8859-1 codificados com UTF-8 em ISO-8859-1 de byte único 

			// ------------------ PORQUE CONCATENOU AQUI? -------------------------

			// vai fazer a decodificação das variaveis em valor trocando os espaços vazios por !@#
			$v .= utf8_decode("" . str_replace(" ", "!@#", $valor) . " ");

		// caso não caia no if acima, faremos o mesmo processo, com alguns passos de segurança 
		else $v .= utf8_decode("'" . str_replace(" ", "!@#", anti_sql_injection($valor)) . "' ");

		// ------------------ PORQUE CASO O VALOR NÃO SEJA NULO OU IGUAL A NOW TEMOS ETAPAS DE SEGURANÇA A MAIS -------------------------

	}

	// trocamos os espaços em branco por ", " e fazemos um trim para remover espaços e outros caracteres 
	$v = str_replace(" ", ", ", trim($v));

	// trocamos !@# por espaços na variavel v
	$v = str_replace("!@#", " ", $v);

	// se o echo for true, vamos dar um echo da inserção nos campos "c" os valores "v"
	if ($echo) echo "INSERT INTO " . $GLOBALS["pre"] . $tabela . "(" . $c . ") VALUES(" . $v . ");<br /><br />";

	// faremos a inserção na tabela passada no parametro, dos valores "v" nas tabelas "c"
	$ins = mysqli_query($GLOBALS["db"], "INSERT INTO " . $GLOBALS["pre"] . $tabela . "(" . $c . ") VALUES(" . $v . ");");

	// igual a parte de cima, ele nos retorno o id se o mesmo foi requerido
	if (mysqli_affected_rows($GLOBALS["db"]) != -1) {

		if ($retornarId) {

			if ($echo) echo "SELECT LAST_INSERT_ID();";

			$sel = mysqli_query($GLOBALS["db"], "SELECT LAST_INSERT_ID() AS id;");

			$fet = mysqli_fetch_array($sel);

			$id = $fet['id'];

			if ($echo) echo " -- " . $id . ";<br /><br />";
		}

		if ($retornarId) return $id;

		else return true;
	}

	return false;
}

/**

 * Altera informações no banco de dados.

 *

 * @param string $query - A query que você deseja para alterar no banco de dados

 * @param bool $echo - Mostra a query inserida - @default false

 *

 * @return bool - Retorna se deu certo ou errado a query.

 */

function alterarPadrao($query, $echo = false)
{

	if ($echo) echo $query . "<br /><br />";

	$upd = mysqli_query($GLOBALS["db"], $query);

	if (mysqli_affected_rows($GLOBALS["db"]) != -1) {

		return true;
	} else return false;
}

/**

 * Altera informações no banco de dados.

 *

 * @param string $tabela - O nome da tabela onde vai inserir

 * @param array(string) $campos - Os campos que serão alterados na base de dados

 * @param array(string) $valores - Os valores dos campos que serão alterados na base de dados (Deve ter a mesma 

	quantidade do array da variavél de campos)

 * @param string $where - 'WHERE' da query - @default null	

 * @param bool $echo - Mostra a query inserida - @default false

 *

 * @return bool - Retorna se deu certo ou errado a query.

 */

function alterar($tabela, $campos, $valores, $where = '', $echo = false)
{

	$v = '';

	for ($i = 0; $i < count($campos); $i++) {

		if (!is_numeric($valores[$i]))

			if ($valores[$i] == '')

				$valores[$i] = 'NULL';

		if ($valores[$i] == 'now()' || $valores[$i] == 'NULL' || strpos($valores[$i], 'DATE_ADD') !== false)

			$v .= $campos[$i] . "=" . utf8_decode(str_replace(" ", "!@#", $valores[$i])) . " ";

		else

			$v .= $campos[$i] . "='" . utf8_decode(str_replace(" ", "!@#", anti_sql_injection($valores[$i]))) . "' ";
	}

	$v = str_replace(" ", ",", trim($v));

	$v = str_replace("!@#", " ", $v);

	if ($echo) echo "UPDATE " . $GLOBALS["pre"] . $tabela . " SET " . $v . ($where != '' ? " WHERE " . $where : "") . ";<br /><br />";

	$upd = mysqli_query($GLOBALS["db"], "UPDATE " . $GLOBALS["pre"] . $tabela . " SET " . $v . ($where != '' ? " WHERE " . $where : "") . ";");

	if (mysqli_affected_rows($GLOBALS["db"]) != -1) {

		return true;
	} else return false;
}

/**

 * Altera informação do campo blob no banco de dados.

 *

 * @param string $tabela - O nome da tabela onde vai inserir

 * @param string $campos - O campo blob que será alterado na base de dados

 * @param string $valores - O valor do campo blob que será alterados na base de dados

 * @param string $where - 'WHERE' da query - @default null

 * @param bool $echo - Mostra a query inserida - @default false

 *

 * @return bool - Retorna se deu certo ou errado a query.

 */


// ------------------ OQUE É UM CAMPO BLOB --------------------------- 
// ------------------ PORQUE NÃO TEMOS OS DOIS TIPOS DE INSERÇÃO NO CAMPO BLOB --------------------------- 

function alterarBlob($tabela, $campo, $valor, $where = '', $echo = false)
{

	if ($valor == '') $valor = 'NULL';

	if ($echo) echo "UPDATE " . $GLOBALS["pre"] . $tabela . " SET " . $campo . " = " . ($valor == 'NULL' ? "NULL" : "'" . $valor . "'") . ($where != '' ? " WHERE " . $where : "") . ";<br /><br />";

	$upd = mysqli_query($GLOBALS["db"], "UPDATE " . $GLOBALS["pre"] . $tabela . " SET " . $campo . " = " . ($valor == 'NULL' ? "NULL" : "'" . $valor . "'") . ($where != '' ? " WHERE " . $where : "") . ";");

	if (mysqli_affected_rows($GLOBALS["db"]) != -1) {

		return true;
	} else return false;
}

/**

 * Exclui informações no banco de dados.

 *

 * @param string $query - A query que você deseja para excluir no banco de dados

 * @param bool $echo - Mostra a query inserida - @default false

 * @param bool $log - Insere um log no banco de dados - @default true

 *

 * @return bool - Retorna se deu certo ou errado a query.

 */

// ------------------ OQUE É ESSE PARAMETRO JUERI --------------------------- 

// function excluirPadrao($query, $echo = false, $jueri = false, $log = true)
// {

// 	if ($echo) echo $query;

// 	// vai receber uma query para deletar algo no db
// 	$del = mysqli_query($GLOBALS["db"], $query);

// 	if (mysqli_affected_rows($GLOBALS["db"]) != -1) {

// 		// se log for true, vamos inserir um log no banco de dados 
// 		if (!$jueri && $log) {

// 			$queryLog = "INSERT INTO change_log(tabela,tipo,data,fk_usuario_id,fk_cliente_sistema_id,observacao) 

// 				VALUES(NULL,'delete',now()," . $_SESSION["dados"]["s_usuario_id"] . ",

// 				" . $_SESSION["dados"]["s_cliente_sistema_id"] . ",'" . anti_sql_injection($query) . "');";

// 			if ($echo) echo $queryLog . "<br /><br />";

// 			$insLog = mysqli_query($GLOBALS["db"], $queryLog);

// 			// se nenhuma fileira for afetada retornamos falso 
// 			if (mysqli_affected_rows($GLOBALS["db"] == -1)) return false;
// 		}

// 		return true;
// 	} else return false;
// }

/**

 * Exclui informações no banco de dados.

 *

 * @param string $tabela - O nome da tabela onde vai excluir

 * @param string $where - 'WHERE' da query - @default null	

 * @param bool $echo - Mostra a query inserida - @default false

 *

 * @return bool - Retorna se deu certo ou errado a query.

 */

function excluir($tabela, $where = '', $echo = false)
{

	if ($echo) echo "DELETE FROM " . $GLOBALS["pre"] . $tabela . ($where != '' ? " WHERE " . $where : "") . ";<br /><br />";

	$del = mysqli_query($GLOBALS["db"], "DELETE FROM " . $GLOBALS["pre"] . $tabela . ($where != '' ? " WHERE " . $where : "") . ";");

	// se echo for true, mostra o numero de fileiras afetadas
	if ($echo) echo mysqli_affected_rows($GLOBALS["db"]);
	if (mysqli_affected_rows($GLOBALS["db"]) != -1) {
		return true;
	} else return false;
}

/**

 * Seleciona informações no banco de dados.

 *

 * @param string $select - A query que você deseja para selecionar no banco de dados

 * @param bool $echo - Mostra a query inserida - @default false

 *

 * @return array(string) - Retorna um array com as informações da $query passada

 */

function selecionar($select, $echo = false)
{

	/* quando for colocar o nome das tabelas com o nome simples, favor tirar essas duas linhas */

	$select = str_replace("FROM ", "FROM " . $GLOBALS["pre"], $select);

	$select = str_replace("from ", "from " . $GLOBALS["pre"], $select);



	$select = str_replace("JOIN ", "JOIN " . $GLOBALS["pre"], $select);

	$select = str_replace("join ", "join " . $GLOBALS["pre"], $select);



	// verifica a primeira ocorrencia da string select em $GLOBALS["pre"]

	if (strpos($select, $GLOBALS["pre"] . "(") !== false) {

		// caso ele encontre uma ocorrencia, faz um replace de $GLOBALS["pre"]."(" por "("
		$select = str_replace($GLOBALS["pre"] . "(", "(", $select);
	}

	// ------------------ QUAL A NECESSIDADE DESSES PASSOS?---------------------------- 

	// ATE AQUI!


	// fazemos a query 
	$sl = mysqli_query($GLOBALS["db"], utf8_decode($select));

	if ($echo) echo $select . '<br /><br />';

	// retorna o numero de fileiras na variavel sl
	$numeroLinha = @mysqli_num_rows($sl);



	$retorno = array();


	// se o numero de linhas for mairo que zero 
	if ($numeroLinha > 0) {

		$i = 0;

		// Busca uma linha de resultado como associativa, matriz numérica ou ambos
		while ($ft = mysqli_fetch_array($sl)) {

			foreach ($ft as $chave => $valor)

				// se a chave não for int, salvamos no array retorno a chave 
				if (!is_int($chave)) $retorno[$i][$chave] = $chave != "foto" ? utf8_encode($valor) : $valor;

			$i++;
		}
	}

	return $retorno;
}

// /**
//  * Seleciona informações no banco de dados para o uso da grid do JWidgets.
//  *
//  * @param string $select - A query que você deseja para selecionar no banco de dados
//  * @param array $campos - As colunas que você deseja mostrar na grid
//  * @param array $pageValues - É usado apenas se usar a $paginacao. 
// 	Os valores que devem ser passados são: 
// 		$pageValues['pagenum'] = $_GET['pagenum'];
// 		$pageValues['pagesize'] = $_GET['pagesize']; - @default array(empty)
//  * @param array $sortValues - É usado apenas se usar a $ordenacao. 
// 	Os valores que devem ser passados são: 
// 		$sortValues['sortdatafield'] = $_GET['sortdatafield'];
// 		$sortValues['sortorder'] = $_GET['sortorder']; - @default array(empty)
//  * @param array $filterValues - É usado apenas se usar a $filtro. 
// 	Os valores que devem ser passados são: 
// 		$filterValues['filterscount'] = $_GET['filterscount'];
// 		$filterValues['filtervalue'] = $_GET['filtervalue']; 
// 		$filterValues['filterdatafield'] = $_GET['filterdatafield']; - @default array(empty)		
//  * @param bool $echo - Mostra a query inserida - @default false
//  * @param bool $virtual - Caso use a grid em modo virtual - @default true
//  * @param bool $paginacao - Caso use a grid com paginação - @default true
//  * @param bool $ordenacao - Caso use a grid com ordenação - @default true
//  * @param bool $filtro - Caso use a grid com filtro - @default true
//  *
//  * @return array(string) - Retorna um array com as informações da $query passada
//  */
// function selecionarJqxGrid(
// 	$select,
// 	$campos,
// 	$pageValues = array(),
// 	$sortValues = array(),
// 	$filterValues = array(),
// 	$echo = false,
// 	$virtual = true,
// 	$paginacao = true,
// 	$ordenacao = true,
// 	$filtro = true,
// 	$groupbyid = true
// ) {

// 	$jueri = false;
// 	/* quando for colocar o nome das tabelas com o nome simples, favor tirar essas duas linhas */
// 	$select = str_replace("FROM ", "FROM " . $GLOBALS["pre"], $select);
// 	$select = str_replace("from ", "from " . $GLOBALS["pre"], $select);

// 	$select = str_replace("JOIN ", "JOIN " . $GLOBALS["pre"], $select);
// 	$select = str_replace("join ", "join " . $GLOBALS["pre"], $select);

// 	if (strpos($select, $GLOBALS["pre"] . "(") !== false) {
// 		$select = str_replace($GLOBALS["pre"] . "(", "(", $select);
// 	}
// 	// ATE AQUI!

// 	$query = str_replace(";", "", $select);

// 	if ($paginacao) {
// 		$pagenum = $pageValues['pagenum'];
// 		$pagesize = $pageValues['pagesize'];
// 		$start = $pagenum * $pagesize;
// 	}

// 	if ($virtual) {
// 		$query = str_replace("SELECT", "SELECT SQL_CALC_FOUND_ROWS", $query);
// 	}

// 	if ($filtro) {
// 		if ($filterValues['filterscount'] != 0) {
// 			if (strpos($query, "WHERE") === false) $query .= " WHERE (";
// 			else $query .= " AND (";

// 			$isOr = false;

// 			for ($i = 0; $i < $filterValues['filterscount']; $i++) {
// 				if (isset($filterValues['filterdatafield'][$i])) {
// 					$apelido = '';
// 					$index = strpos($query, $filterValues['filterdatafield'][$i]);
// 					if ($index !== false) {
// 						if (substr($query, $index - 1, 1) == ".") {
// 							$novoIndex = $index - 2;
// 							for ($j = $novoIndex; $j > 0; $j--) {
// 								if (substr($query, $j, 1) != " ") {
// 									$apelido .= substr($query, $j, 1);
// 								} else {
// 									break;
// 								}
// 							}

// 							$apelido = strrev($apelido) . ".";
// 						} else if (strtolower(substr($query, $index - 3, 2)) == "as") {
// 							$novoIndex = $index - 5;
// 							for ($j = $novoIndex; $j > 0; $j--) {
// 								if (substr($query, $j, 1) != " ") {
// 									$apelido .= substr($query, $j, 1);
// 								} else {
// 									break;
// 								}
// 							}

// 							$apelido = strrev($apelido);
// 							$filterValues['filterdatafield'][$i] = "";
// 						}
// 					}

// 					if ($filterValues["filtertype"][$i] == "OR" && !$isOr) {
// 						$query .= " (";
// 						$isOr = true;
// 					}
// 					$query .= " " . $apelido . $filterValues['filterdatafield'][$i] . " LIKE '%" . anti_sql_injection(str_replace(" ", "%", $filterValues['filtervalue'][$i])) . "%'";

// 					if ((trim($filterValues["filtertype"][$i]) == "AND" && $isOr)) {
// 						$query .= ")";
// 						$isOr = false;
// 					}

// 					if (isset($filterValues["filtertype"][$i]) && !empty($filterValues["filtertype"][$i])) {
// 						if ($i + 1 < $filterValues['filterscount']) {
// 							$query .= " " . trim($filterValues["filtertype"][$i]);
// 						}
// 					}
// 				}
// 			}

// 			$query .= ")";
// 		}
// 	}
// 	if ($groupbyid) {
// 		$query .= " GROUP BY id ";
// 	}
// 	if ($ordenacao) {
// 		if ($sortValues['sortdatafield'] != "") {
// 			if ($sortValues['sortorder'] == "desc") {
// 				$query .= " ORDER BY " . $sortValues['sortdatafield'] . " DESC";
// 			} else {
// 				$query .= " ORDER BY " . $sortValues['sortdatafield'] . " ASC";
// 			}
// 		}
// 	}


// 	if ($paginacao) {
// 		$query .= " LIMIT " . $start . ", " . $pagesize;
// 	}

// 	if ($virtual) {
// 		if ($echo) echo utf8_decode($query) . '<br /><br />';
// 		$result = mysqli_query(!$jueri ? $GLOBALS["db"] : $GLOBALS["dbJueri"], utf8_decode($query)) or die("SQL Error 1: " . mysqli_error());

// 		$sql = "SELECT FOUND_ROWS() AS found_rows;";
// 		if ($echo) echo $sql . '<br /><br />';
// 		$rows = mysqli_query(!$jueri ? $GLOBALS["db"] : $GLOBALS["dbJueri"], $sql);
// 		$rows = mysqli_fetch_assoc($rows);
// 		$total_rows = $rows['found_rows'];
// 	} else {
// 		if ($echo) echo $query . '<br /><br />';
// 		$result = mysqli_query(!$jueri ? $GLOBALS["db"] : $GLOBALS["dbJueri"], $query) or die("SQL Error 1: 234" . mysqli_error());
// 	}

// 	$customers = array();

// 	$i = 0;

// 	while ($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
// 		foreach ($campos as $campo) {
// 			$customers[$i][$campo] = $row[$campo] != "" ? utf8_encode($row[$campo]) : "-";
// 		}
// 		$i++;
// 	}

// 	if ($virtual) {
// 		$data = array(
// 			'totalRows' => $total_rows,
// 			'rows' => $customers
// 		);
// 	} else {
// 		$data = $customers;
// 	}

// 	return $data;
// }

/**

 * Começa uma transição na base de dados (Sessão).

 *

 * @param bool $echo - Mostra a query inserida - @default false

 *

 * @return null

 */

function startTransaction($echo = false, $jueri = false)
{

	if ($echo) echo "START TRANSACTION;<br /><br />";

	$sl = mysqli_query($GLOBALS["db"], "START TRANSACTION;");
}

// /**

//  * Finaliza a transição fazendo as alterações na base de dados (Para usar essa função, 

// 	você tem que começar uma transição).

//  *

//  * @param bool $echo - Mostra a query inserida - @default false

//  *

//  * @return null

//  */

// function commit($echo = false, $jueri = false)
// {

// 	if ($echo) echo "COMMIT;<br /><br />";

// 	$sl = mysqli_query($GLOBALS["db"], "COMMIT;");
// }

// /**

//  * Finaliza a transição ignorando tudo que foi feito durante (Para usar essa função, 

// 	você tem que começar uma transição - Não insere nada no banco de dados).

//  *

//  * @param bool $echo - Mostra a query inserida - @default false

//  *

//  * @return null

//  */

function rollback($echo = false, $jueri = false)
{

	if ($echo) echo "ROLLBACK;<br /><br />";

	$sl = mysqli_query($GLOBALS["db"], "ROLLBACK;");
}

/**

 * Tira tudo que é errado ao fazer uma alteração no banco de dados, impossibilitando de usar o SQL Injection

 *

 * @param string $str - A string que deseja ser verificada

 *

 * @return string - A string verificada para não usar o SQL Injection

 */

// function anti_sql_injection($str)
// {

// 	if (!is_numeric($str)) {

// 		//$str = get_magic_quotes_gpc() ? stripslashes($str) : $str;

// 		$str = function_exists('mysqli_real_escape_string') ? mysqli_real_escape_string($GLOBALS["db"], $str) : mysql_escape_string($str);
// 	}

// 	return $str;
// }

/**

 * Fecha a conexão do BD depois de usa-lo.

 *

 * @param bool $echo - Mostra o que o mysql fechou - @default false

 *

 * @return null

 */

function fecharMysql($echo = false, $jueri = false)
{

	if ($echo) echo "MYSQL CLOSE;<br /><br />";

	$result = mysqli_close(!$jueri ? $GLOBALS["db"] : $GLOBALS["dbJueri"]);
}
