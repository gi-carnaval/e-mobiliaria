<?

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require "funcoes-banco.php";

function head(){
?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="../template/dark-light/assets/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="../template/dark-light/assets/images/favicon.png" type="image/x-icon">

    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins" rel="stylesheet">

    <!-- themify -->
    <link rel="stylesheet" type="text/css" href="../template/dark-light/assets/icon/themify-icons/themify-icons.css">

    <!-- iconfont -->
    <link rel="stylesheet" type="text/css" href="../template/dark-light/assets/icon/icofont/css/icofont.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="../template/dark-light/assets/css/font-awesome.min.css">

    <!-- simple line icon -->
    <link rel="stylesheet" type="text/css" href="../template/dark-light/assets/icon/simple-line-icons/css/simple-line-icons.css">

    <!-- Required Framework -->
    <link rel="stylesheet" type="text/css" href="../template/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- MULTI SELECT -->
    <link rel="stylesheet" href="../template/bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css" />
    <link rel="stylesheet" href="../template/bower_components/multiselect/css/multi-select.css" />
    <link rel="stylesheet" href="../template/bower_components/select2/dist/css/select2.min.css" />

    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="../template/dark-light/assets/css/main.css">

    <!-- Responsive.css-->
    <link rel="stylesheet" type="text/css" href="../template/dark-light/assets/css/responsive.css">

    <script src="../template/bower_components/Jquery/dist/jquery.min.js"></script>
    <script src="../template/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script src="../template/bower_components/tether/dist/js/tether.min.js"></script>

    <script type="text/javascript" src="../js/jquery.livejquery.js"></script>

    <!-- Form validation -->
    <script type="text/javascript" src="../js/newValidacao.js"></script>

    <!-- Required Framework -->
    <script src="../template/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Scrollbar JS-->
    <script src="../template/bower_components/jquery-slimscroll/jquery.slimscroll.js"></script>
    <script src="../template/bower_components/jquery.nicescroll/dist/jquery.nicescroll.min.js"></script>

    <!-- classic JS-->
    <script src="../template/bower_components/classie/classie.js"></script>

    <!-- notification -->
    <script src="../template/dark-light/assets/plugins/notification/js/bootstrap-growl.min.js"></script>

    <!-- Counter js  -->
    <script src="../template/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="../template/dark-light/assets/plugins/countdown/js/jquery.counterup.js"></script>

    <!-- SWEETALERT -->
    <script src="../node_modules/sweetalert2/dist/sweetalert2.all.min.js"></script> <!-- NOVO SWEETALERT 2-->

    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="../template/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="../template/dark-light/assets/plugins/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../template/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">

    <!-- data-table js -->
    <script src="../template/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../template/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../template/dark-light/assets/plugins/data-table/js/jszip.min.js"></script>
    <script src="../template/dark-light/assets/plugins/data-table/js/pdfmake.min.js"></script>
    <script src="../template/dark-light/assets/plugins/data-table/js/vfs_fonts.js"></script>
    <script src="../template/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../template/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../template/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../template/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../template/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.11.3/sorting/date-euro.js" type="text/javascript"></script>
    <!-- <script src="../js/any-number.js"></script> -->
    <script src="../js/datatable.js?v=3.4"></script>

    <!-- tooltip -->
    <script src="../js/tooltip.js"></script>

    <!-- dateTime -->
    <script src="../template/dark-light/assets/plugins/datepicker/js/moment-with-locales.min.js"></script>
    <!-- <script src="../template/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <link rel="stylesheet" href="../template/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" /> -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- custom -->
    <!-- <link rel="stylesheet" type="text/css" href="../css/newStyle.css?v=1.5"> -->
    <script src="../js/custom.js?v=1.3"></script>
    <script src="../js/modal-visual.js?v1.5"></script>
    <link rel="stylesheet" type="text/css" href="../css/style.css?v0.3" />
<?}?>
