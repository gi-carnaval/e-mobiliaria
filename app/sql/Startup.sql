
CREATE DATABASE `emobiliaria`CHARACTER SET latin1 COLLATE latin1_swedish_ci; 

CREATE TABLE `emobiliaria`.`type_user`(  
  `id_type_user` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250),
  PRIMARY KEY (`id_type_user`)
) ENGINE=INNODB CHARSET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE `emobiliaria`.`user`(  
  `id_user` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NOT NULL,
  `email` VARCHAR(250) NOT NULL,
  `cpf` VARCHAR(250) NOT NULL,
  `password` VARCHAR(2000) NOT NULL,
  `id_type_user` INT(11) UNSIGNED,
  `active` TINYINT(1) DEFAULT 1,
  `created_at` DATETIME,
  `updated_at` DATETIME,
  PRIMARY KEY (`id_user`),
  CONSTRAINT `user_type_user_fk` FOREIGN KEY (`id_type_user`) REFERENCES `emobiliaria`.`type_user`(`id_type_user`)
) ENGINE=INNODB CHARSET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE `emobiliaria`.`real_estate`(  
  `id_real_estate` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fantasy_name` VARCHAR(250) NOT NULL,
  `corporate_name` VARCHAR(250) NOT NULL,
  `email` VARCHAR(250) NOT NULL,
  `cnpj` VARCHAR(250) NOT NULL,
  `password` VARCHAR(2000) NOT NULL,
  `id_user` INT(11) UNSIGNED,
  `cep` INT(8),
  `uf` VARCHAR(2) NOT NULL,
  `city` VARCHAR(250) NOT NULL,
  `district` VARCHAR(250) NOT NULL,
  `address` VARCHAR(250) NOT NULL,
  `complement` VARCHAR(250) NOT NULL,
  `active` TINYINT(1) DEFAULT 1,
  `created_at` DATETIME,
  `updated_at` DATETIME,
  PRIMARY KEY (`id_real_estate`),
  CONSTRAINT `real_estate_user` FOREIGN KEY (`id_user`) REFERENCES `emobiliaria`.`user`(`id_user`)
) ENGINE=INNODB CHARSET=latin1 COLLATE=latin1_swedish_ci;   

CREATE TABLE `emobiliaria`.`immobile`(  
  `id_immobile` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NOT NULL,
  `cep` INT(8) NOT NULL,
  `uf` VARCHAR(2) NOT NULL,
  `city` VARCHAR(250) NOT NULL,
  `district` VARCHAR(250) NOT NULL,
  `address` VARCHAR(250) NOT NULL,
  `complement` VARCHAR(250) NOT NULL,
  `value` REAL(11,2) NOT NULL,
  `bedrooms` INT(2) NOT NULL,
  `bathrooms` INT(2) NOT NULL,
  `square_meters` REAL(11,2) NOT NULL,
  `garage` INT(2) NOT NULL,
  `description` VARCHAR(2000) NOT NULL,
  `id_real_estate` INT(11) UNSIGNED,
  `active` TINYINT(1) NOT NULL,
  `created_at` DATETIME,
  `updated_at` DATETIME,
  PRIMARY KEY (`id_immobile`),
  CONSTRAINT `immobile_real_estate` FOREIGN KEY (`id_real_estate`) REFERENCES `emobiliaria`.`real_estate`(`id_real_estate`)
) ENGINE=INNODB CHARSET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE `emobiliaria`.`attributes`(  
  `id_attributes` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250),
  PRIMARY KEY (`id_attributes`)
) ENGINE=INNODB CHARSET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE `emobiliaria`.`immobile_attributes`(  
  `id_immobile` INT(11) UNSIGNED,
  `id_attributes` INT(11) UNSIGNED,
  CONSTRAINT `immobile_attributes_immobile_fk` FOREIGN KEY (`id_immobile`) REFERENCES `emobiliaria`.`immobile`(`id_immobile`),
  CONSTRAINT `immobile_attributes_attributes` FOREIGN KEY (`id_attributes`) REFERENCES `emobiliaria`.`attributes`(`id_attributes`)
) ENGINE=INNODB CHARSET=latin1 COLLATE=latin1_swedish_ci;